<?php
// var_dump('hello_world');


// Disable Woocommerce main styles
add_filter('woocommerce_enqueue_styles', '__return_false');


/**
 * Disable WooCommerce block styles (front-end).
 * https://themesharbor.com/disabling-css-styles-of-woocommerce-blocks/
 */
function slug_disable_woocommerce_block_styles() {
  wp_dequeue_style( 'wc-block-style' );
}
add_action( 'wp_enqueue_scripts', 'slug_disable_woocommerce_block_styles' );



/**
 * Disable WooCommerce block styles (back-end).
 * https://themesharbor.com/disabling-css-styles-of-woocommerce-blocks/
 */
function slug_disable_woocommerce_block_editor_styles() {
  wp_deregister_style( 'wc-block-editor' );
  wp_deregister_style( 'wc-block-style' );
}
add_action( 'enqueue_block_assets', 'slug_disable_woocommerce_block_editor_styles', 1, 1 );



// ADD ACF FIELDS TO WOOCOMMERCE ATTRIBUTES

// Adds a custom rule type.
add_filter( 'acf/location/rule_types', function( $choices ){
    $choices[ __("Other",'acf') ]['wc_prod_attr'] = 'WC Product Attribute';
    return $choices;
} );

// Adds custom rule values.
add_filter( 'acf/location/rule_values/wc_prod_attr', function( $choices ){
    foreach ( wc_get_attribute_taxonomies() as $attr ) {
        $pa_name = wc_attribute_taxonomy_name( $attr->attribute_name );
        $choices[ $pa_name ] = $attr->attribute_label;
    }
    return $choices;
} );

// Matching the custom rule.
add_filter( 'acf/location/rule_match/wc_prod_attr', function( $match, $rule, $options ){
    if ( isset( $options['taxonomy'] ) ) {
        if ( '==' === $rule['operator'] ) {
            $match = $rule['value'] === $options['taxonomy'];
        } elseif ( '!=' === $rule['operator'] ) {
            $match = $rule['value'] !== $options['taxonomy'];
        }
    }
    return $match;
}, 10, 3 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 50 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 0 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

remove_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices', 10 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_output_all_notices', 40 );




// remove option edit title wishlist
if( ! function_exists( 'yith_wcwl_disable_title_editing' ) ){
	function yith_wcwl_disable_title_editing( $params ) {
		$params['can_user_edit_title'] = false;

		return $params;
	}
	add_filter( 'yith_wcwl_wishlist_params', 'yith_wcwl_disable_title_editing' );
}




//remove title form products!!!! page
remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10); // Remove title form original position
// remove image/thumbnail from products page 
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
// remove prive form products page
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

add_action('woocommerce_shop_loop_item_title', 'abChangeProductsTitle', 10); // Insert title in new position ( out of main container )
function abChangeProductsTitle()
{
    global $post;
    global $product; 
    $currency = get_woocommerce_currency_symbol();

    $pid = $product->get_id();
    $currency = get_woocommerce_currency_symbol();
    $add_to_wishlist = do_shortcode('[yith_wcwl_add_to_wishlist product_id=' . $pid . ']' );

    $category =  wp_get_post_terms($post->ID, 'product_cat');
    if (!empty($category)) {
        $categoryItem = $category[0];
        $nameCat = $categoryItem->name;
    }
echo ' <div class="product__info info">';
echo '<h3 class="category-name">';
echo '' . $nameCat . '';
echo '</h3>';
echo '<div class="product_name_area">';
echo '<h2 class="product__name">';
echo the_title();
echo '</h2>';
echo '<div class="description_item_product">';
echo apply_filters( 'woocommerce_short_description', $product->get_short_description() );
echo '<div class="icon_cart">';
echo '<svg id="Group_8" data-name="Group 8" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14.027" height="17.675" viewBox="0 0 14.027 17.675">   <defs <rect="" id="Rectangle_4" data-name="Rectangle 4" width="14.027" height="17.675" fill="#474747"></defs>      <g id="Group_7" data-name="Group 7" clip-path="url(#clip-path)">     <path id="Path_21" data-name="Path 21" d="M12.257,121.544H1.77A1.768,1.768,0,0,1,0,119.779q0-.045,0-.091l.477-10.158a1.768,1.768,0,0,1,1.768-1.691H11.78a1.768,1.768,0,0,1,1.768,1.691l.477,10.158a1.768,1.768,0,0,1-1.677,1.854l-.091,0m-10.01-12.52a.589.589,0,0,0-.589.589l-.477,10.134a.589.589,0,0,0,.559.618H12.257a.589.589,0,0,0,.59-.588c0-.01,0-.02,0-.03l-.477-10.157A.589.589,0,0,0,11.78,109Z" transform="translate(0 -103.869)" fill="#474747"></path>     <path id="Path_22" data-name="Path 22" d="M101.53,4.566h-1.178V3.535a2.357,2.357,0,0,0-4.713,0V4.566H94.46V3.535a3.535,3.535,0,0,1,7.07,0Z" transform="translate(-90.982 0)" fill="#474747"></path>   </g> </svg>';
echo ' <p class="product__price">';
echo $product->get_price();
echo $currency;
echo '</p>';

echo '</div>';
echo '</div>';
echo '</div>';
echo '</div>';
};


// disable automatic responsive style for whislist
add_filter('yith_wcwl_is_wishlist_responsive', '__return_false');
add_filter('yith_wcwl_localize_script', function ($loc) {
    $loc['actions']['load_mobile_action'] = 'no-mob-ver';
    return $loc;
});
add_filter('yith_wcwl_template_part_hierarchy', function ($data) {
    foreach ($data as $k => $v) $data[$k] = str_replace('-mobile.php', '.php', $v);
    return $data;
});


// remove orginal woocommerce ordering. Yu need add "woocommerce_catalog_ordering()"
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );




// deleted add to cart button from products and category page
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);





// remove breadcrumbs from /sklep
add_action('template_redirect', 'remove_shop_breadcrumbs' );
function remove_shop_breadcrumbs(){
 
    if (is_shop() || is_archive())
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
 
}

add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'override_woocommerce_image_size_gallery_thumbnail' );
function override_woocommerce_image_size_gallery_thumbnail( $size ) {
    // Gallery thumbnails: proportional, max width 200px
    return array(
        'width'  => 300,
        'height' => 300,
        'crop'   => 0,
    );
}


// add link for popup sizes product
add_action( 'woocommerce_before_add_to_cart_button', 'misha_before_add_to_cart_btn' );
function misha_before_add_to_cart_btn(){ ?>


<?php if( get_field('wlacz_nazwa_przycisku_uruchamiajacego_popup_z_rozmiarami') ) { ?>
<div id="myBtn"><?php the_field('nazwa_przycisku_uruchamiajacego_popup_z_rozmiarami','options') ?></div>
<?php } ?>
<div id="myModal"
    class="modal">
    <div class="modal-content">


        <div class="tabs-area-size">
            <a class="tabs-bar-item tab-button"
                onclick="openCity('table-size')"><?php the_field('tabela_rozmiarow_tytul_przycisku','options') ?></a>
            <a class="tabs-bar-item tab-button"
                onclick="openCity('info-size')"><?php the_field('jak_sie_mierzyc_tytul_przycisku','options') ?></a>
        </div>

        <div id="table-size"
            class="tab-container">
            <?php  the_field('tabela_rozmiarow_produktu'); ?>
        </div>

        <div id="info-size"
            class="tab-container how-size-area"
            style="display:none">
            <?php the_field('jak_sie_mierzyc_opis') ?>
        </div>

        <div class="contact-modal-area">
            <h3><?php the_field('tabela_rozmiarow_masz_pytania','options') ?></h3>
            <a href="<?php the_field('tabela_rozm_kontakt_link_przycisku','options') ?>"
                class="btn btn-contact-modal"><?php the_field('tabela_rozm_kontakt_tytul_przycisku','options') ?></a>
        </div>
        <span class="close">&times;</span>
    </div>
</div>
<?php }



function wc_remove_checkout_fields( $fields ) {

    // Billing fields
    unset( $fields['billing']['billing_state'] );
    // Shipping fields
    unset( $fields['shipping']['shipping_state'] );
    
    return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'wc_remove_checkout_fields' );


add_filter( 'woocommerce_get_script_data', 'change_view_cart', 10, 2 );
function change_view_cart( $params, $handle ) {
  if ( $handle == 'wc-add-to-cart' ) {
    $params['i18n_view_cart'] = "Dodano do koszyka. Zobacz koszyk lub kontynuuj zakupy.";
  }
  
  return $params;
}

add_action( 'wp_footer' , 'custom_quantity_fields_script' );
function custom_quantity_fields_script(){
    ?>
<script type='text/javascript'>
jQuery(function($) {

    // Quantity "plus" and "minus" buttons
    $(document.body).on('click', '.plus, .minus', function() {
        var $qty = $(this).closest('.quantity').find('.qty'),
            currentVal = parseFloat($qty.val()),
            max = parseFloat($qty.attr('max')),
            min = parseFloat($qty.attr('min')),
            step = $qty.attr('step');

        // Format values
        if (!currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
        if (max === '' || max === 'NaN') max = '';
        if (min === '' || min === 'NaN') min = 0;
        if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN')
            step = 1;

        // Change the value
        if ($(this).is('.plus')) {
            if (max && (currentVal >= max)) {
                $qty.val(max);
            } else {
                $qty.val((currentVal + parseFloat(step)).toFixed(step.getDecimals()));
            }
        } else {
            if (min && (currentVal <= min)) {
                $qty.val(min);
            } else if (currentVal > 0) {
                $qty.val((currentVal - parseFloat(step)).toFixed(step.getDecimals()));
            }
        }

        // Trigger change event
        $qty.trigger('change');
    });
});
</script>
<?php
}





/* Display Cart @ Checkout Page Only - WooCommerce */
add_action( 'woocommerce_before_checkout_form', 'bbloomer_cart_on_checkout_page_only', 5 );
function bbloomer_cart_on_checkout_page_only() {
if ( is_wc_endpoint_url( 'order-received' ) ) return;
echo do_shortcode('[woocommerce_cart]');
}



// change url cart to order
function change_cart_url_to_checkout() {
    return wc_get_checkout_url();
 }
 add_filter('woocommerce_get_cart_url', 'change_cart_url_to_checkout', 10, 1 );



//  move coupon to after subtotal
 remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
 add_action( 'woocommerce_review_order_after_cart_contents', 'woocommerce_checkout_coupon_form_custom' );
 function woocommerce_checkout_coupon_form_custom() {
     echo '<tr class="coupon-form"><td colspan="2">';
     
     wc_get_template(
         'checkout/form-coupon.php',
         array(
             'checkout' => WC()->checkout(),
         )
     );
     echo '</tr></td>';
 }

 
?>
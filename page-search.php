<?php
/*
 * Template Name: Search Template
 */
get_header();
?>

<div class="top-space"></div>

<div class="search-form">
    <?php echo do_shortcode('[fibosearch]'); ?>
    <h3>Zacznij pisać, aby zobaczyć produkty, których szukasz.</h3>
</div>

<?php
get_footer();?>
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
  * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
  *
  * @package WordPress
  * @subpackage Twenty_Seventeen
  * @since Twenty Seventeen 1.0
  * @version 1.0
*/
  
  get_header(); ?>

<div class="top-space"></div>

<main class="basic">


    <!-- Section Breadcrumbs -->
    <?php get_template_part( 'template-parts/commons/common', 'breadcrumbs' ); ?>

    <!-- Section Info Right -->
    <?php get_template_part( 'template-parts/commons/common', 'infoRight' ); ?>


    <!-- the_content -->
    <section class="content">
        <div class="container">

            <h1 class="page-title"><?php the_title(); ?></h1>

            <?php the_content(); ?>
        </div>
    </section>
</main>


<?php get_footer(); ?>
<section class="newsletter">
    <div class="container">
        <div class="title">
            <h2><?php the_field('naglowek_newsletter','options') ?></h2>
            <div class="form-area-newsletter">
                <form class="form-area-newsletter-input" action="<?php get_home_url(); ?>/?na=s" method="post">
                    <div class="input-group flex-form-newsletter">
                        <div class="tnp-field tnp-field-email">
                            <input type="email" class="tnp-email form-control" name="ne" value="" required
                                placeholder="<?php the_field('placeholder_newsletter','options') ?>">
                        </div>

                        <label>
                            <input type="checkbox" name="ny" required class="check-yes"><span class="rodo">
                                <?php the_field('tekst_akceptacji_regulaminu_newsletter','options') ?>
                                <div class="input-group-appen tnp-field tnp-field-submit">
                                    <button class="btn-send-newlsetter"
                                        type="submit"><?php the_field('tekst_przycisku_newsletter','options') ?></button>
                                </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="info-icons">
    <div class="container">
        <div class="info-icons-home-wrapperr">
            <?php if( have_rows('icons_infoicon_list','options') ):
                      while( have_rows('icons_infoicon_list','options') ) : the_row(); ?>

            <div class="block-item">
                <div class="infoicon_img">
                    <img src="<?php the_sub_field('img_infoicon_homepage','options'); ?>" />
                </div>
                <div class="title">
                    <h4><?php the_sub_field('title_infoicon_homepage','options') ?></h4>
                </div>
            </div>

            <?php 
                endwhile;
                else :
                endif; ?>
        </div>
        <?php 
// get the current taxonomy term
$term = get_queried_object();


// vars
$naglowek_collapse = get_field('head_text_collpase', $term);
$text_collapse = get_field('text_collpase_shop', $term);
?>


        <?php if( isset( $naglowek_collapse ) ): ?>
        <div class="text-shop-info">




            <details>
                <summary><?php echo $naglowek_collapse;  ?></summary>
                <?php echo $text_collapse; ?>
            </details>
        </div>
        <?php endif; ?>

    </div>
</section>
<!doctype html>
<html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="format-detection"
            content="telephone=no" />
        <meta name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>
            <?php wp_title(); ?>
        </title>
        <link rel="pingback"
            href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

        <?php wp_head(); ?>

    </head>

    <body>

        <header>
            <!-- if homepage add class navbarHome -->
            <nav class="navbar navbar-expand-lg navbar-toggleable-md navbar-inverse fixed-top nav-subpage">
                <div class="container">
                    <div class="header-wrapper">
                        <!-- custom logo start -->
                        <?php 
                    $custom_logo_id = get_theme_mod( 'custom_logo' );
                     $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                                    ?>
                        <a class="navbar-brand"
                            href="<?php echo esc_url(home_url('/')); ?>"
                            title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
                            rel="home"> <img src="<?php echo $image[0]; ?>"
                                alt=""></a>
                        <!--  custom logo stop -->

                        <!-- Collapse button burger menu Start-->
                        <button class="navbar-toggler first-button"
                            type="button"
                            data-toggle="collapse"
                            data-target="#navbarNavDropdown"
                            aria-controls="navbarSupportedContent20"
                            aria-expanded="false"
                            aria-label="Toggle navigation">
                            <div class="animated-icon1"><span></span><span></span><span></span></div>
                        </button>
                        <!-- Collapse button burger menu End-->
                        <div class="two-menu-wrap">
                            <!-- start menu primary  -->


                            <div class="menu-acf menu-acf-desktop">
                                <?php if( get_field('menu_odnosniki','options') ): ?>
                                <?php while( the_repeater_field('menu_odnosniki','options') ): ?>
                                <div class="menu-acf-single-kat">
                                    <a href="<?php the_sub_field('menu_main_odnosnik_link','options');?>">
                                        <p class="menu-main-a"><?php the_sub_field('menu_main_odnosnik','options');?>
                                        </p>
                                    </a>
                                    <?php if( get_sub_field('menu_main_podkategorie_true','options') ): ?>
                                    <div class="menu-display">

                                        <?php if( have_rows('menu_kat','options') ): ?>
                                        <?php while( have_rows('menu_kat','options') ): the_row();?>
                                        <div class="menu-display-columns">
                                            <div class="column column-1">
                                                <?php if( have_rows('dodaj_podkategorie','options') ): ?>
                                                <?php while( have_rows('dodaj_podkategorie','options') ): the_row();?>
                                                <a href="<?php the_sub_field('link_podkategorii','options');?>">
                                                    <p><?php the_sub_field('nazwa_podkategorii','options');?></p>
                                                </a>
                                                <?php endwhile; ?>
                                                <?php endif; ?>
                                            </div>
                                            <div class="column column-2">
                                                <?php if( get_sub_field('dodaj_podkategorie_true') ): ?>
                                                <?php if( have_rows('dodaj_podkategorie_kopia','options') ): ?>
                                                <?php while( have_rows('dodaj_podkategorie_kopia','options') ): the_row();?>
                                                <a href="<?php the_sub_field('link_podkategorii_1','options');?>">
                                                    <p><?php the_sub_field('nazwa_podkategorii_1','options');?></p>
                                                </a>
                                                <?php endwhile; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php if( get_sub_field('dodaj_zdjecie_true') ): ?>
                                                <a href="<?php the_sub_field('menu_kat_odnosnik_link','options');?>">
                                                    <img src="<?php the_sub_field('menu_kat_odnosnik','options');?>" />
                                                </a>
                                                <?php endif; ?>

                                            </div>
                                            <div class="column column-3">
                                                <a href="<?php the_sub_field('dodaj_zdjecie_2_link','options');?>">
                                                    <img src="<?php the_sub_field('dodaj_zdjecie_2','options');?>" />
                                                </a>
                                            </div>
                                        </div>


                                        <?php endwhile; ?>
                                        <?php endif; ?>

                                    </div>
                                    <?php endif; ?>
                                </div>
                                <?php endwhile; ?>
                                <?php endif; ?>
                            </div>

                            <div class="menu-acf-mobile"
                                id="navbarNavDropdown">
                                <?php if( get_field('menu_odnosniki','options') ): ?>
                                <?php while( the_repeater_field('menu_odnosniki','options') ): ?>
                                <div class="menu-acf-single-kat">
                                    <details>
                                        <summary class="kat-title">
                                            <a href="<?php the_sub_field('menu_main_odnosnik_link','options');?>">
                                                <p class="menu-main-a">
                                                    <?php the_sub_field('menu_main_odnosnik','options');?>
                                                </p>
                                            </a>
                                            <img src="/wp-content/themes/mcpremium/assets/src/img/icon.jpg"
                                                alt="open arrow" />
                                        </summary>
                                        <?php if( get_sub_field('menu_main_podkategorie_true','options') ): ?>
                                        <div class="menu-display">
                                            <?php if( have_rows('menu_kat','options') ): ?>
                                            <?php while( have_rows('menu_kat','options') ): the_row();?>
                                            <div class="menu-display-columns">
                                                <div class="column column-1">
                                                    <?php if( have_rows('dodaj_podkategorie','options') ): ?>
                                                    <?php while( have_rows('dodaj_podkategorie','options') ): the_row();?>
                                                    <a href="<?php the_sub_field('link_podkategorii','options');?>">
                                                        <p><?php the_sub_field('nazwa_podkategorii','options');?></p>
                                                    </a>
                                                    <?php endwhile; ?>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="column column-2">
                                                    <?php if( get_sub_field('dodaj_podkategorie_true') ): ?>
                                                    <?php if( have_rows('dodaj_podkategorie_kopia','options') ): ?>
                                                    <?php while( have_rows('dodaj_podkategorie_kopia','options') ): the_row();?>
                                                    <a href="<?php the_sub_field('link_podkategorii_1','options');?>">
                                                        <p><?php the_sub_field('nazwa_podkategorii_1','options');?></p>
                                                    </a>
                                                    <?php endwhile; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php if( get_sub_field('dodaj_zdjecie_true') ): ?>
                                                    <a
                                                        href="<?php the_sub_field('menu_kat_odnosnik_link','options');?>">
                                                        <img
                                                            src="<?php the_sub_field('menu_kat_odnosnik','options');?>" />
                                                    </a>
                                                    <?php endif; ?>

                                                </div>
                                                <div class="column column-3">
                                                    <a href="<?php the_sub_field('dodaj_zdjecie_2_link','options');?>">
                                                        <img
                                                            src="<?php the_sub_field('dodaj_zdjecie_2','options');?>" />
                                                    </a>
                                                </div>
                                            </div>


                                            <?php endwhile; ?>
                                            <?php endif; ?>

                                        </div>
                                        <?php endif; ?>
                                    </details>

                                </div>
                                <?php endwhile; ?>
                                <?php endif; ?>
                            </div>


                            <!-- end menu primary  -->


                            <!-- start menu icon -->
                            <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'primary_right',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => '',
                        'container_id'      => 'navbarNavDropdown-icons',
                        'menu_class'        => 'nav navbar-nav',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            =>  new WP_Bootstrap_Navwalker(),
                    ) );
                ?>
                            <!-- end menu icon -->
                        </div>

                    </div>
                </div>
            </nav>
        </header>
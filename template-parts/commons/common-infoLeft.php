<!-- ACF -> common info left -->
<!--

Exist on pages: 
- home page
- basic template
 
-->


<?php 
$infoLeft = get_field('show_info_left_section'); 
if  ( $infoLeft ) {
?>


<section class="info left">
    <div class="container left">
        <div class="title__wrap left">
            <h2 class="section__title info__title title left"><?php the_field('info_left_tytul') ?></h2>
        </div>

        <div class="info__content section__content content text"><?php the_field('info_left_tresc') ?></div>
    </div>
</section>


<?php } ?>
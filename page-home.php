<?php
/*
 * Template Name: Homepage template
 */
get_header();
?>
<main id="homepage">
    <section class="hero">
        <?php $imgHeroHomepage = get_field('zdjecie_w_tle_naglowek_home'); ?>

        <?php if( $imgHeroHomepage ): ?>

        <img class="visible-desctop"
            src="<?php echo $imgHeroHomepage['sizes']['homepage-hero-desctop']; ?>"
            alt="<?php echo $imgHeroHomepage['alt']; ?>" />

        <img class="visible-tablet"
            src="<?php echo $imgHeroHomepage['sizes']['homepage-hero-tablet']; ?>"
            alt="<?php echo $imgHeroHomepage['alt']; ?>" />

        <img class="visible-phone"
            src="<?php echo $imgHeroHomepage['sizes']['homepage-hero-phone']; ?>"
            alt="<?php echo $imgHeroHomepage['alt']; ?>" />
        <?php endif; ?>

        <div class="container">
            <div class="wrap-info-hero">
                <div class="titleAndCategory">
                    <p><?php the_field('nazwa_kategorii_hero_homepage') ?></p>
                    <h3><?php the_field('nazwa_produktu_hero_homepage') ?></h3>
                </div>
                <?php if( get_field('tekst_przycisku_hero_homepage') ): ?>
                <a href="<?php the_field('link_przycisku_hero_homepage') ?>"
                    class="btn btn-shop"><?php the_field('tekst_przycisku_hero_homepage') ?></a>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <section class="look-home">
        <div class="container">
            <div class="look-home-wrapperr">
                <?php if( have_rows('produkty_lookbook_lista') ):
                      while( have_rows('produkty_lookbook_lista') ) : the_row(); ?>
                <a href="<?php the_sub_field('link_produktu_lookbook_homepage') ?>"
                    class="product__card product">
                    <div class="product__img">
                        <?php $imgLookBook = get_sub_field('zdjecie_produktu_lookbook_homepage'); ?>
                        <?php if( $imgLookBook ): ?>
                        <img src="<?php echo $imgLookBook['sizes']['img-rectangle-tablet']; ?>"
                            alt="<?php echo $imgLookBook['alt']; ?>" />
                        <?php endif; ?>
                    </div>
                </a>
                <?php 
                endwhile;
                else :
                endif; ?>
            </div>
            <div class="wrap-btn-right">
                <?php if( get_field('tekst_przycisku_look_home') ): ?>
                <a href="<?php the_field('link_przycisku_look_home') ?>"
                    class="btn btn-shop"><?php the_field('tekst_przycisku_look_home') ?></a>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <section class="special-collection1">
        <div class="container">
            <div class="wrapper-special-collection1">
                <div class="left-column">
                    <div class="image-collection">
                        <?php $imgCollection1 = get_field('zdjecie_kolekcji_specjalnej1'); ?>
                        <?php if( $imgCollection1 ): ?>
                        <img src="<?php echo $imgCollection1['sizes']['img-square-phone']; ?>"
                            alt="<?php echo $imgCollection1['alt']; ?>" />
                        <?php endif; ?>



                    </div>
                    <div class="titleAndCategory">
                        <p><?php the_field('nazwa_kategorii_kolekcji_specjalnej_homepage') ?></p>
                        <h3><?php the_field('nazwa_kolekcji_specjalnej_homepage') ?></h3>
                    </div>
                </div>
                <div class="right-column">
                    <div class="image-logo-collection">
                        <?php $imgCollection1Logo = get_field('zdjecie_kolekcji_specjalne_first'); ?>
                        <?php if( $imgCollection1Logo ): ?>
                        <img src="<?php echo $imgCollection1Logo['sizes']['homepage-hero-phone']; ?>"
                            alt="<?php echo $imgCollection1Logo['alt']; ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="wrap-btn-center">
                        <?php if( get_field('tekst_przycisku_kolekcji_specjalnej1') ): ?>
                        <a href="<?php the_field('link_przycisku_kolekcji_specjalnej1') ?>"
                            class="btn btn-shop"><?php the_field('tekst_przycisku_kolekcji_specjalnej1') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php if ( get_field('film_youtube_link')): ?>
    <section class="movie-homepage">
        <div class="container">
            <div class="video-container">
                <iframe width="100%"
                    height="625"
                    src="<?php the_field('film_youtube_link') ?>"
                    title="MC Premium YouTube video player"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
            </div>
        </div>
    </section>
    <?php endif; ?>


    <?php if( get_field('lista_kolekcji_dodatkowej_homepage') ): ?>
    <section class="look-home custom-section-imgages-products">
        <div class="container">
            <div class="look-home-wrapperr">
                <?php if( have_rows('lista_kolekcji_dodatkowej_homepage') ):
                      while( have_rows('lista_kolekcji_dodatkowej_homepage') ) : the_row(); ?>
                <a href="<?php the_sub_field('link_do_produktu_kolekcji_dodatkowej') ?>"
                    class="product__card product">
                    <div class="product__img">
                        <?php $imgLookBook2 = get_sub_field('zdjecie_produktu_kolekcji dodatkowej'); ?>
                        <?php if( $imgLookBook2 ): ?>
                        <img src="<?php echo $imgLookBook2['sizes']['img-rectangle-tablet']; ?>"
                            alt="<?php echo $imgLookBook2['alt']; ?>" />
                        <?php endif; ?>
                    </div>
                </a>
                <?php 
                endwhile;
                else :
                endif; ?>
            </div>
            <div class="wrap-btn-right">
                <?php if( get_field('nazwa_przycisku_wiecej_kolekcji_dodatkowej') ): ?>
                <a href="<?php the_field('link_przycisku_wiecej_kolekcji_dodatkowej') ?>"
                    class="btn btn-shop"><?php the_field('nazwa_przycisku_wiecej_kolekcji_dodatkowej') ?></a>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <?php endif; ?>




    <section class="accessories">
        <div class="container">
            <div class="accessories-home-wrapper">
                <?php if( have_rows('akcesoria_i_dodatki_homepage_lista') ):
                      while( have_rows('akcesoria_i_dodatki_homepage_lista') ) : the_row(); ?>
                <a href="<?php the_sub_field('link_produktu_akcesoria_i_dodatki_homepage') ?>"
                    class="product__card product">
                    <div class="product__img">
                        <?php $imgAccessories = get_sub_field('zdjecie_produktu_akcesoria_i_dodatki_homepage'); ?>
                        <?php if( $imgAccessories ): ?>
                        <img src="<?php echo $imgAccessories['sizes']['img-rectangle-tablet']; ?>"
                            alt="<?php echo $imgAccessories['alt']; ?>" />
                        <?php endif; ?>
                    </div>
                </a>
                <?php 
                endwhile;
                else :
                endif; ?>
            </div>
            <div class="wrap-info-hero">
                <div class="titleAndCategory">
                    <p><?php the_field('nazwa_kategorii_accessories_homepage') ?></p>
                    <h3><?php the_field('nazwa_produktu_accessories_homepage') ?></h3>
                </div>
                <?php if( get_field('tekst_przycisku_accessories_homepage') ): ?>
                <a href="<?php the_field('link_przycisku_accessories_homepage') ?>"
                    class="btn btn-shop"><?php the_field('tekst_przycisku_accessories_homepage') ?></a>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <section class="newsletter">
        <div class="container">
            <div class="title">
                <h2><?php the_field('naglowek_newsletter','options') ?></h2>
                <div class="form-area-newsletter">
                    <form class="form-area-newsletter-input"
                        action="<?php get_home_url(); ?>/?na=s"
                        method="post">
                        <div class="input-group flex-form-newsletter">
                            <div class="tnp-field tnp-field-email">
                                <input type="email"
                                    class="tnp-email form-control"
                                    name="ne"
                                    value=""
                                    required
                                    placeholder="<?php the_field('placeholder_newsletter','options') ?>">
                            </div>

                            <label>
                                <input type="checkbox"
                                    name="ny"
                                    required
                                    class="check-yes"><span class="rodo">
                                    <?php the_field('tekst_akceptacji_regulaminu_newsletter','options') ?>
                                    <div class="input-group-appen tnp-field tnp-field-submit">
                                        <button class="btn-send-newlsetter"
                                            type="submit"><?php the_field('tekst_przycisku_newsletter','options') ?></button>
                                    </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="info-icons">
        <div class="container">
            <div class="info-icons-home-wrapperr">
                <?php if( have_rows('icons_infoicon_list','options') ):
                      while( have_rows('icons_infoicon_list','options') ) : the_row(); ?>

                <div class="block-item">
                    <div class="infoicon_img">
                        <img src="<?php the_sub_field('img_infoicon_homepage','options'); ?>" />
                    </div>
                    <div class="title">
                        <h4><?php the_sub_field('title_infoicon_homepage','options') ?></h4>
                    </div>
                </div>

                <?php 
                endwhile;
                else :
                endif; ?>
            </div>

            <div class="text-shop-info">
                <details>
                    <summary><?php the_field('head_text_collpase') ?></summary>
                    <?php the_field('text_collpase_shop') ?>
                </details>
            </div>

        </div>
    </section>

</main>

<?php
get_footer();
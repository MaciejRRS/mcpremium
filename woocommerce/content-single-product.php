<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;
global $product;
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );
if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>"
    class="single-product-class"
    <?php wc_product_class( '', $product ); ?>>
    <div class="row">
        <div class="col-lg-6">
            <?php
				/**
				 * Hook: woocommerce_before_single_product_summary.
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action( 'woocommerce_before_single_product_summary' );
				?>
        </div>
        <div class="col-lg-6">
            <div class="summary entry-summary">
                <?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' );
					?>

                <div class="add-inform-area">
                    <?php if( get_field("opis_produktu_true") ): ?>
                    <details open>
                        <summary class="title">
                            <h6><?php the_field("opis_produktu_tytul");?></h6>
                            <img src="/wp-content/themes/mcpremium/assets/src/img/icon.jpg"
                                alt="open arrow" />
                        </summary>
                        <div class="content">
                            <?php the_content();?>
                        </div>
                    </details>
                    <?php endif; ?>

                    <?php if( get_field("wymiary_produktu_true") ): ?>
                    <details>
                        <summary class="title">
                            <h6><?php the_field("wymiary_produktu_tytul");?></h6>
                            <img src="/wp-content/themes/mcpremium/assets/src/img/icon.jpg"
                                alt="open arrow" />
                        </summary>
                        <div class="content">
                            <?php the_field("wymiary_produktu_text");?>
                        </div>
                    </details>
                    <?php endif; ?>

                    <?php if( get_field("reguly_produktu_true") ): ?>
                    <details>
                        <summary class="title">
                            <h6><?php the_field("reguly_produktu_tytul");?></h6>
                            <img src="/wp-content/themes/mcpremium/assets/src/img/icon.jpg"
                                alt="open arrow" />
                        </summary>
                        <div class="clothes-wash">
                            <?php 
                            $produkt = get_field('produkt_reguly');
                            if( $produkt && in_array('pranie_30', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__maximum washing temperature 30°C  normal process.svg"/>';
                            } if( $produkt && in_array('pranie_30_d', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__maximum washing temperature 30°C mild process.svg"/>';
                            } if( $produkt && in_array('pranie_30_bd', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__maximum washing temperature 30°C very mild process.svg"/>';
                            } if( $produkt && in_array('pranie_40', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__maximum washing temperature 40°C  normal process.svg"/>';
                            } if( $produkt && in_array('pranie_40_d', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__ maximum washing temperature 40°C mild process.svg"/>';
                            } if( $produkt && in_array('pranie_40_bd', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__maximum washing temperature 40°C very mild process.svg"/>';
                            } if( $produkt && in_array('pranie_50', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__maximum washing temperature 50°C  normal process.svg"/>';
                            } if( $produkt && in_array('pranie_50_d', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__maximum washing temperature 50°C mild process.svg"/>';
                            } if( $produkt && in_array('pranie_60', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__maximum washing temperature 60°C  normal process.svg"/>';
                            } if( $produkt && in_array('pranie_60_d', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__maximum washing temperature 60°C mild process.svg"/>';
                            } if( $produkt && in_array('pranie_70', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__maximum washing temperature 70°C  normal process.svg"/>';
                            } if( $produkt && in_array('pranie_95', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__maximum washing temperature 95°C  normal process.svg"/>';
                            } if( $produkt && in_array('pranie_r', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__wash by hand maximum temperature 40°C.svg"/>';
                            } if( $produkt && in_array('pranie_n', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__do not wash.svg"/>';
                            } if( $produkt && in_array('prof_1', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__professional dry cleaning in hydrocarbons normal process.svg"/>';
                            } if( $produkt && in_array('prof_2', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__professional dry cleaning in tetrachloroethene and all solvents (for F) normal process.svg"/>';
                            } if( $produkt && in_array('prof_3', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__professional wet cleaning normal process.svg"/>';
                            } if( $produkt && in_array('susz_1', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__tumble drying possible drying at lower temperature maximum exhaust temperature 60°C.svg"/>';
                            } if( $produkt && in_array('susz_2', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__tumble drying possible normal temperature maximum exhaust temperatue 80°C.svg"/>';
                            } if( $produkt && in_array('susz_3', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__line drying.svg"/>';
                            } if( $produkt && in_array('susz_4', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__line drying in the shade.svg"/>';
                            } if( $produkt && in_array('susz_5', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__flat drying.svg"/>';
                            } if( $produkt && in_array('susz_6', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__flat drying in the shade.svg"/>';
                            } if( $produkt && in_array('susz_7', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__do not tumble dry.svg"/>';
                            } if( $produkt && in_array('susz_8', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__do not dry clean.svg"/>';
                            } if( $produkt && in_array('wybielacz', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__any bleaching agent allowed.svg"/>';
                            } if( $produkt && in_array('wybielacz_tl', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__only oxygen - non-chlorine bleach allowed.svg"/>';
                            } if( $produkt && in_array('wybielacz_n', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__do not bleach.svg"/>';
                            } if( $produkt && in_array('pras_1', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__iron at a maximum sole-plate temperature of 110°C without steam.svg"/>';
                            } if( $produkt && in_array('pras_2', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__ iron at a maximum sole-plate temperature of 150°C.svg"/>';
                            } if( $produkt && in_array('pras_3', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__iron at a maximum sole-plate temperature of 200°C.svg"/>';
                            } if( $produkt && in_array('pras_4', $produkt) ) {
                                echo '<img src="/wp-content/themes/mcpremium/assets/src/img/laundry_symbols/laundry_symbol__do not iron.svg"/>';
                            } 
                            ;?>
                        </div>
                    </details>
                    <?php endif; ?>

                    <?php if( get_field("dostawa_produktu_true") ): ?>
                    <details>
                        <summary class="title">
                            <h6><?php the_field("dostawa_produktu_tytul");?></h6>
                            <img src="/wp-content/themes/mcpremium/assets/src/img/icon.jpg"
                                alt="open arrow" />
                        </summary>
                        <div class="content">
                            <?php the_field("dostawa_produktu_text");?>
                        </div>
                    </details>
                    <?php endif; ?>

                </div>
            </div>
        </div>
        <div class="col-lg-12 additional-products">
            <h3 class="additional"> <?php the_field("uzupelnij_tytul");?> </h3>
            <div class="additional-block">
                <div class="block-left">
                    <img src="<?php the_field("uzupelnij_image");?>" />
                </div>
                <div class="block-right">
                    <div class="block-product">
                        <?php $featured_post = get_field('wybierz_produkt');
								$featured_category = get_the_category( $featured_post);
								if( $featured_post ): ?>

                        <?php echo '<a href="' . get_permalink( $featured_post ) . '">';?>
                        <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
                        <?php echo get_the_post_thumbnail( $featured_post, 'large' );?>
                        <div class="product__cat">
                            <p><?php
								$terms = get_the_terms($featured_post, 'product_cat');
									foreach ($terms as $term) {

										$product_cat = $term->name;
										echo $product_cat;
											break;
								}
								?></p>
                        </div>
                        <h2 class="product__name"><?php echo esc_html( $featured_post->post_title ); ?></h2>
                        <div class="product__price">
                            <svg id="Group_8"
                                data-name="Group 8"
                                xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="14.027"
                                height="17.675"
                                viewBox="0 0 14.027 17.675">
                                <defs <rect=""
                                    id="Rectangle_4"
                                    data-name="Rectangle 4"
                                    width="14.027"
                                    height="17.675"
                                    fill="#474747"></defs>
                                <g id="Group_7"
                                    data-name="Group 7"
                                    clip-path="url(#clip-path)">
                                    <path id="Path_21"
                                        data-name="Path 21"
                                        d="M12.257,121.544H1.77A1.768,1.768,0,0,1,0,119.779q0-.045,0-.091l.477-10.158a1.768,1.768,0,0,1,1.768-1.691H11.78a1.768,1.768,0,0,1,1.768,1.691l.477,10.158a1.768,1.768,0,0,1-1.677,1.854l-.091,0m-10.01-12.52a.589.589,0,0,0-.589.589l-.477,10.134a.589.589,0,0,0,.559.618H12.257a.589.589,0,0,0,.59-.588c0-.01,0-.02,0-.03l-.477-10.157A.589.589,0,0,0,11.78,109Z"
                                        transform="translate(0 -103.869)"
                                        fill="#474747"></path>
                                    <path id="Path_22"
                                        data-name="Path 22"
                                        d="M101.53,4.566h-1.178V3.535a2.357,2.357,0,0,0-4.713,0V4.566H94.46V3.535a3.535,3.535,0,0,1,7.07,0Z"
                                        transform="translate(-90.982 0)"
                                        fill="#474747"></path>
                                </g>
                            </svg>
                            <?php 
							$product = new WC_Product($featured_post); 
							echo wc_price($product->get_price_including_tax(1,$product->get_price()));
							?>
                        </div>

                        <?php echo '<div class="product__excerpt">' . get_the_excerpt($featured_post). '</div>';?>
                        <?php echo '</a>';?>
                        <?php endif; ?>
                    </div>
                    <div class="block-product">
                        <?php $featured_post = get_field('wybierz_produkt_2');
								$featured_category = get_the_category( $featured_post);
								if( $featured_post ): ?>
                        <?php echo '<a href="' . get_permalink( $featured_post ) . '">';?>
                        <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
                        <?php echo get_the_post_thumbnail( $featured_post, 'large' );?>
                        <div class="product__cat">
                            <p><?php
								$terms = get_the_terms($featured_post, 'product_cat');
									foreach ($terms as $term) {

										$product_cat = $term->name;
										echo $product_cat;
											break;
								}
								?></p>
                        </div>
                        <h2 class="product__name"><?php echo esc_html( $featured_post->post_title ); ?></h2>
                        <div class="product__price">
                            <svg id="Group_8"
                                data-name="Group 8"
                                xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="14.027"
                                height="17.675"
                                viewBox="0 0 14.027 17.675">
                                <defs <rect=""
                                    id="Rectangle_4"
                                    data-name="Rectangle 4"
                                    width="14.027"
                                    height="17.675"
                                    fill="#474747"></defs>
                                <g id="Group_7"
                                    data-name="Group 7"
                                    clip-path="url(#clip-path)">
                                    <path id="Path_21"
                                        data-name="Path 21"
                                        d="M12.257,121.544H1.77A1.768,1.768,0,0,1,0,119.779q0-.045,0-.091l.477-10.158a1.768,1.768,0,0,1,1.768-1.691H11.78a1.768,1.768,0,0,1,1.768,1.691l.477,10.158a1.768,1.768,0,0,1-1.677,1.854l-.091,0m-10.01-12.52a.589.589,0,0,0-.589.589l-.477,10.134a.589.589,0,0,0,.559.618H12.257a.589.589,0,0,0,.59-.588c0-.01,0-.02,0-.03l-.477-10.157A.589.589,0,0,0,11.78,109Z"
                                        transform="translate(0 -103.869)"
                                        fill="#474747"></path>
                                    <path id="Path_22"
                                        data-name="Path 22"
                                        d="M101.53,4.566h-1.178V3.535a2.357,2.357,0,0,0-4.713,0V4.566H94.46V3.535a3.535,3.535,0,0,1,7.07,0Z"
                                        transform="translate(-90.982 0)"
                                        fill="#474747"></path>
                                </g>
                            </svg>
                            <?php 
							$product = new WC_Product($featured_post); 
							echo wc_price($product->get_price_including_tax(1,$product->get_price()));
							?>
                        </div>

                        <?php echo '<div class="product__excerpt">' . get_the_excerpt($featured_post). '</div>';?>
                        <?php echo '</a>';?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
				/**
				 * Hook: woocommerce_after_single_product_summary.
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_upsell_display - 15
				 * @hooked woocommerce_output_related_products - 20
				 */
				do_action( 'woocommerce_after_single_product_summary' );
				// global $product;
				// $pid = $product->get_id();
				// echo do_shortcode('[product_price]')
				?>

    </div>

    <?php do_action( 'woocommerce_after_single_product' ); ?>
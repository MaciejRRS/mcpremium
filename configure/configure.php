<?php
/* Enable support for custom logo. */

	add_theme_support( 'custom-logo', array(
		'height'      => 400,
		'width'       => 100,
		'flex-height' => true,
		'flex-width'  => true,
	) );


	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'mcpremium' ),
		'primary_right' => __( 'Primary Ikony', 'mcpremium' ),
		'primary_left' => __( 'Primary Language', 'mcpremium' ),
	) );



function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


// Register thumbnails
	add_theme_support( 'post-thumbnails' );
    add_image_size( 'homepage-thumb', 385, 302 ); // Soft Crop Mode
	add_image_size( 'homepage-hero-desctop', 1920, 1080 ); // Soft Crop Mode
	add_image_size( 'homepage-hero-tablet', 1199, 767 ); // Soft Crop Mode
	add_image_size( 'homepage-hero-phone', 767, 450 ); // Soft Crop Mode

	add_image_size( 'img-rectangle-desctop', 1200, 800 ); // Soft Crop Mode
	add_image_size( 'img-rectangle-tablet', 800, 650 ); // Soft Crop Mode
	add_image_size( 'img-rectangle-phone', 767, 550 ); // Soft Crop Mode

	add_image_size( 'img-square-phone', 990, 767 ); // Soft Crop Mode


//add option page to panel (ACF)
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
	acf_add_options_page('Menu');
	acf_add_options_page('Stopka');
	acf_add_options_page('Przyciski tabeli rozmiarów');
}




function wpb_widgets_init() {
 
    register_sidebar( array(
        'name' => __( 'Widget filrtowanie', 'mcpremium' ),
        'id' => 'sidebar-filter',
        'description' => __( 'The main sidebar appears on the right on each page except the front page template', 'wpb' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
    }
 
add_action( 'widgets_init', 'wpb_widgets_init' );
?>
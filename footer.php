<footer class="footer">
    <div class="footer-tresc-wrap">
        <div class="container">
            <div class="wrap-grid-footer">
                <div class="footer-logo-wrap">
                    <img src="<?php the_field('glowne_logo_strony','option' ) ?>"
                        alt="MC Premium"
                        class="logo-footer">
                </div>

                <div class="wrap-columns-footer">
                    <div class="column-footer">
                        <h4 class="tytul-columny">
                            <?php the_field('naglowek_dane_adresowe_stopka','option' ) ?>
                        </h4>
                        <?php the_field('adres_text_adress_footer', 'option') ?>
                    </div>

                    <div class="column-footer">
                        <h4 class="tytul-columny">
                            <?php the_field('naglowek_kategorie_footer','option' ) ?>
                        </h4>
                        <?php if( have_rows('kategorie_lista_linkow_footer', 'option') ): ?>
                        <ul>
                            <?php while ( have_rows('kategorie_lista_linkow_footer','option') ) : the_row(); ?>
                            <li class="linki-stopki"><a
                                    href="<?php the_sub_field('link_kategorii_footer','option' ) ?>"><?php the_sub_field('nazwa_kategorii_footer','option' ) ?></a>
                            </li>
                            <?php endwhile; ?>
                        </ul>
                        <?php endif; ?>
                    </div>

                    <div class="column-footer">
                        <h4 class="tytul-columny">
                            <?php the_field('o_sklepie_neglowek','option' ) ?>
                        </h4>
                        <?php if( have_rows('lista_linkow_o_sklepie', 'option') ): ?>
                        <ul>
                            <?php while ( have_rows('lista_linkow_o_sklepie','option') ) : the_row(); ?>
                            <li class="linki-stopki"><a
                                    href="<?php the_sub_field('link_zakladki_o_sklepie','option' ) ?>"><?php the_sub_field('nazwa_zakladki_o_sklepie','option' ) ?></a>
                            </li>
                            <?php endwhile; ?>
                        </ul>
                        <?php endif; ?>
                    </div>

                    <div class="column-footer">
                        <h4 class="tytul-columny">
                            <?php the_field('naglowek_pole_z_kontaktem_footer','option' ) ?>
                        </h4>
                        <?php the_field('pole_z_kontaktem_footer','option' ) ?>




                        <div class="column-footer in-column-footer">
                            <h4 class="tytul-columny">
                                <?php the_field('naglowek_social_media_footer','option' ) ?>
                            </h4>
                            <?php if( have_rows('lista_ikon_social_media_footer', 'option') ): ?>
                            <ul>
                                <?php while ( have_rows('lista_ikon_social_media_footer','option') ) : the_row(); ?>
                                <li class="linki-social-media-footer"><a
                                        href="<?php the_sub_field('link_social_media_footer','option' ) ?>"><img
                                            src="<?php the_sub_field('ikona_social_media_footer','option' ) ?>"
                                            alt=""></a>
                                </li>
                                <?php endwhile; ?>
                            </ul>
                            <?php endif; ?>
                        </div>



                    </div>


                </div>
            </div>
        </div>
    </div>






</footer>

<?php wp_footer(); ?>

<script>
$(function() {
    $(window).on('load scroll resize orientationchange', function() {
        var scroll = $(window).scrollTop();
        var $win = $(window);
        if (scroll >= 120) {
            $(".navbar").addClass("scroll");
        } else {
            $(".navbar").removeClass("scroll");
        }
    });
});
</script>


<script>
$(window).on('load resize', function() {
    var viewportWidth = $(window).width();
    if (viewportWidth < 1024) {
        $(".berocket_inline_filters").addClass("collapse");
        $(".berocket_ajax_group_filter_title").click(function() {
            $(".berocket_inline_filters").collapse('toggle'); // toggle collapse
        });
    } else {
        $(".berocket_inline_filters").removeClass("collapse");
    }
});

jQuery(document).ready(function($) {
    console.log("E");
});

jQuery(document).ready(function($) {
    $('#hamburger').click(function() {
        console.log("eee");
    });
});
</script>





</body>

</html>
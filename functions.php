<?php

// CONFIG
include( 'configure/configure.php' );

// JAVASCRIPT & CSS
include( 'configure/js-css-fonts.php' ); 

// Woocommerce
include( 'configure/woocommerce-custom.php' );

// Register Custom Navigation Walker
require_once 'configure/class-wp-bootstrap-navwalker.php';



 // debug for worpdress
 ini_set('display_errors','Off');
 ini_set('error_reporting', E_ALL );
 define('WP_DEBUG', false);
 define('WP_DEBUG_DISPLAY', false);

 

?>
<!-- Subscription Form + Instagram  -->

<!-- Implement on pages:
Home
default template  -->

<section class="social">
    <div class="row mx-0">
        <div class="form col-lg-5 social__part">
            <h2 class="section__title social__title"><?php the_field('form_title', 'options') ?></h2>
            <?php echo do_shortcode( '[contact-form-7 id="164" title="Subscription"]' ); ?>
        </div>
        <div class="instagram col-lg-7 social__part">
            <h2 class="section__title social__title"><?php the_field('instagram_title', 'options')  ?></h2>

        </div>
    </div>
</section>